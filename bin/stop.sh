#!/bin/sh

PID=$(ps -ef | grep ms-mcms.jar | grep -v grep | awk '{ print $2 }')

if [ ! -z "$PID" ]
then
    kill $PID
fi
